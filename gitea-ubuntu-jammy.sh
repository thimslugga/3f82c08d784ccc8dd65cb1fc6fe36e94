#!/usr/bin/env bash

# https://docs.gitea.io/en-us/install-from-binary/
# https://docs.gitea.io/en-us/linux-service/
# https://github.com/go-gitea/gitea/blob/main/contrib/systemd/gitea.service
# https://docs.gitea.io/en-us/config-cheat-sheet/
# https://docs.gitea.io/en-us/backup-and-restore/

# Check if this script was run as a non-root user
function is_root() {
  if [[ ${EUID} -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
  fi
};
is_root

# Check if this script is being run on Ubuntu
function is_ubuntu() {
  local distro
  distro=$(awk '/^ID=/' /etc/*-release | tr -d '"' | awk -F'=' '{ print tolower($2) }')
  
  case "${distro}" in
    ubuntu) echo 'This is Ubuntu Linux' ;;
         *) echo 'This is not Ubuntu Linux.'; exit 1 ;; 
  esac
};
is_ubuntu

mkdir -p /etc/apt/{apt.conf,trusted.gpg,sources.list}.d

# Disable phased updates found in Ubuntu 21.10 and newer
cat <<'EOF' | tee /etc/apt/apt.conf.d/99custom-disable-phased-updates
// To have all your machines phase the same, set the same string in this field
// If commented out, apt will use /etc/machine-id to seed the random number generator
APT::Machine-ID "aaaabbbbccccddddeeeeffff";

// Always include phased updates.
// For example, after your initial build, you would comment this out.
// If left in place you will *always* include phased updates instead of phasing all machines together.
Update-Manager::Always-Include-Phased-Updates;
APT::Get::Always-Include-Phased-Updates: True;

EOF

# Disable apt install of recommended software
cat <<'EOF' | tee /etc/apt/apt.conf.d/99custom-no-install-recommends
// Disable the automatic install of recommended packages.
APT::Install-Recommends "false";

// Disable the install of suggested packages
//APT::Install-Suggests "false";

EOF

# Disable apt advertisements.
command -v pro >/dev/null 2>&1 \
  && pro config set apt_news=false
  
# Disable motd advertisements on login.
test -f /etc/default/motd-news \
  && sed -i -e 's/ENABLED=1/ENABLED=0/g' /etc/default/motd-news
  
# Disable needrestart found in 22.04 and newer as it causes issues for scripts and automation solutions.
test -f /etc/needrestart/needrestart.conf \
  && sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf

# Enable universe repoistory
command -v apt-add-repository >/dev/null 2>&1 && apt-add-repository universe

# Enable multiverse repository
#command -v apt-add-repository >/dev/null 2>&1 && apt-add-repositor multiverse

# Enable restricted repository
#command -v apt-add-repository >/dev/null 2>&1 && apt-add-repositor restricted

apt-get update -y \
  && apt-get -o APT::Get::Always-Include-Phased-Updates=true install -y \
    software-properties-common \
    apt-transport-https \
    wget \
    curl \
    ca-certificates \
    gnupg2 \
    ubuntu-keyring \
    git \
    git-lfs \
    git-secret \
    git-secrets

# Optional install UFW
#apt-get -o APT::Get::Always-Include-Phased-Updates=true install -y ufw

# Optional) install MariaDB
#apt-get -o APT::Get::Always-Include-Phased-Updates=true install -y mariadb-server mariadb-client

#systemctl enable --now mariadb

#mysql_secure_installation
#mysql -u root -p

#CREATE DATABASE giteadb;
#CREATE USER 'giteauser'@'localhost' IDENTIFIED BY 'your_strong_passwd';
#GRANT ALL ON giteadb.* TO 'giteauser'@'localhost' IDENTIFIED BY 'your_strong_passwd' WITH GRANT OPTION;
#FLUSH PRIVILEGES;
#EXIT;

curl -sL 'https://dl.gitea.com/gitea/1.18.5/gitea-1.18.5-linux-amd64' \
  -o /usr/local/bin/gitea \
  && chmod +x /usr/local/bin/gitea
  
gpg2 --keyserver keys.openpgp.org --recv 7C9E68152594688862D62AF62D9AE806EC1592E2
gpg2 --verify gitea-1.18.5-linux-amd64.asc /usr/local/bin/gitea
  
git --version
  
adduser \
   --system \
   --shell /bin/bash \
   --gecos 'Git Version Control' \
   --group \
   --disabled-password \
   --home /home/git \
   git

#sudo -u git ssh-keygen -t rsa -b 4096 -C "Gitea Host Key"
#sudo -u git cat /home/git/.ssh/id_rsa.pub | sudo -u git tee -a /home/git/.ssh/authorized_keys
#sudo -u git chmod 600 /home/git/.ssh/authorized_keys

#cat <<"EOF" | sudo tee /usr/local/bin/gitea
##!/bin/sh
#ssh -p 2222 -o StrictHostKeyChecking=no git@127.0.0.1 "SSH_ORIGINAL_COMMAND=\"$SSH_ORIGINAL_COMMAND\" $0 $@"
#EOF

#chmod +x /usr/local/bin/gitea

mkdir -p /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea

mkdir -p /var/lib/gitea/{custom,data,indexers,public,log}
chown git: /var/lib/gitea/{data,indexers,log}
chmod 750 /var/lib/gitea/{data,indexers,log}
#chown -R git:git /var/lib/gitea/
#chmod -R 750 /var/lib/gitea/

# /etc/gitea is temporarily set with write permissions for user git 
# so that the web installer can write the configuration file. After 
# the installation is finished, it is recommended to set permissions 
# to read-only using.
#chmod 750 /etc/gitea
#chmod 640 /etc/gitea/app.ini

#ufw allow 3000/tcp
#ufw enable
#ufw status

curl -sL 'https://raw.githubusercontent.com/go-gitea/gitea/main/contrib/systemd/gitea.service' \
 -o /etc/system/systemd/gitea.service

systemctl daemon-reload

# https://docs.gitea.io/en-us/environment-variables/
#export GITEA_WORK_DIR=/var/lib/gitea/
GITEA_WORK_DIR=/var/lib/gitea/ /usr/local/bin/gitea web -c /etc/gitea/app.ini

#systemctl start gitea
#systemctl enable --now gitea

#cp contrib/autocompletion/bash_autocomplete /usr/share/bash-completion/completions/gitea